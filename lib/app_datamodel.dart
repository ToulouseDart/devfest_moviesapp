
/**
 * DevFest Toulouse #2017
 * Boris-Wilfried & Kevin
 */

/////////////////////
/////// RESPONSE
/////////////////////
class MoviesResponse {
  String page;
  String totalResults;
  String totalPages;
  List<MovieEntry> searchMovieEntries = [];

  MoviesResponse.fromJson(Map json) {
    this.page = json['page'].toString();
    this.totalResults = json['total_results'].toString();
    this.totalPages = json['total_pages'].toString();
    List results = json['results'];
    results.forEach((entry) {
      searchMovieEntries.add(new MovieEntry.fromJson(entry));
    });
  }
}

//////////////////
////// ENTRY
//////////////////
class MovieEntry {
  String posterPath;
  bool adult;
  String overview;
  String releaseDate;
  List genreIds;
  String id;
  String originalTitle;
  String originalLanguage;
  String title;
  String backdropPath;
  String popularity;
  String voteCount;
  bool video;
  String voteAverage;

  MovieEntry.fromEmpty() {
    originalTitle = "";
  }

  MovieEntry.fromJson(Map json) {
    this.posterPath = json['poster_path'].toString();
    this.adult = json['adult'].toString() == "true";
    this.overview = json['overview'].toString();
    this.id = json['id'].toString();
    this.originalTitle = json['original_title'].toString();
    this.originalLanguage = json['original_language'].toString();
    this.releaseDate = json['release_date'].toString();
    this.title = json['title'].toString();
    this.backdropPath = json['backdrop_path'].toString();
    this.popularity = json['popularity'].toString();
    this.voteCount = json['vote_count'].toString();
    this.video = json['video'].toString() == "true";
    this.voteAverage = json['vote_average'].toString();
  }

  MovieEntry.toJson() {
    Map json = new Map();
    json['original_title'] = originalTitle;
    json['release_date'] = releaseDate;
  }
}

////////////////////////////////////
///////// MOCK Movies /////////////
//////////////////////////////////

Map _mockJson() => {
      "page": "1",
      "total_page": "1",
      "total_results": "3",
      "results": [
        {"original_title": "Ninja à Blagnac",
          "release_date": "25/12/2017",
        },
        {"original_title": "Toulouse en fête",
          "release_date": "04/07/2018",
        },
        {"original_title": "Mrs & Mr Geek",
          "release_date": "02/10/2017",
        },
      ]
    };

MoviesResponse getMockResponse() => new MoviesResponse.fromJson(_mockJson());

