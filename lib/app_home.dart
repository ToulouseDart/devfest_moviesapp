/**
 * DevFest Toulouse #2017
 * Boris-Wilfried & Kevin
 */

import 'package:flutter/material.dart';
import 'app_datamodel.dart';

///
/// Construct HOME PAGE as Stateless Widget using FAKE MOVIES
///
class MyHomePage extends StatelessWidget {

  final String title;

  MyHomePage({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(

      // Build APP BAR
        appBar: new AppBar(
          title: new Text(this.title),
        ),

        // Build BODY
        body: new Container(
          decoration: const BoxDecoration(
            border: const Border(top: const BorderSide(color: Colors.black26)),
          ),
          child: new ListView(
              shrinkWrap: true,
              primary: false,
              children: getMoviesEntryLines()),
        ));
  }
}


///
///  Usage of MergeSemantic Widget
///
List<MergeSemantics> getMoviesEntryLines() {
  List<MergeSemantics> widgets = [];
  getMockResponse().searchMovieEntries.forEach((entry) {

    String title = entry.originalTitle;
    String releaseDate = entry.releaseDate;

    widgets.add(new MergeSemantics(
      child: new ListTile(
        dense: true,
        title: new Text("$title - $releaseDate"),
      ),
    ));
  });
  return widgets;
}
